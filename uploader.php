<?php
session_start();
// Get the filename and make sure it is valid
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	header("Location: FileList.php");
	exit;
}
// Get the username and make sure it is valid
$username = $_SESSION['user'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	//header("Location: FileList.php");
	//exit;
}
$full_path = sprintf("/home/lduan/FileSharing/Users/%s/%s", $username, $filename);
echo "$full_path";
if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
	//echo "YES";
	chmod($full_path, 0777);
	chown($full_path, "lduan");
	chgrp($full_path, "lduan");
	
	header("Location: upload_success.php");
	exit;
}
else{
	//echo "NO";
	header("Location: upload_failure.php");
	exit;
}
?>